import ctypes
import pygame
import json

from line_sweep import count_intersections
from util import Circle, play_move, has_moves_left


# configuration
with open('constants.json') as f:
    CONSTANTS = json.load(f)

pygame.init()
ctypes.windll.user32.SetProcessDPIAware()

clock = pygame.time.Clock()
info = pygame.display.Info()

WHITE = (255, 255, 255)
PLAYER = CONSTANTS['player_color']
AI = CONSTANTS['ai_color']
BLACK = (0, 0, 0)

WIDTH, HEIGHT = info.current_w, info.current_h

screen = pygame.display.set_mode(
    (WIDTH, HEIGHT),
    pygame.FULLSCREEN
)

pygame.display.set_caption('coins')
pygame.display.flip()
font = pygame.font.SysFont('Arial', 30)

# functions
draw_circle = pygame.draw.circle

# variables
table_radius = CONSTANTS['table_radius']
table = Circle((WIDTH // 2, HEIGHT // 2), table_radius)
coins = []
radius = CONSTANTS['coin_radius']

running = True

# ai move
coins.append(Circle(play_move(table, coins), radius))

# game loop
while running:

    screen.fill(WHITE)

    draw_circle(screen, BLACK, (table.center.x, table.center.y), table.radius, 2)

    if not has_moves_left(table, coins, radius):
        surf = font.render('NO MOVES LEFT', False, BLACK)
        screen.blit(surf, (100, 100))

    for i in range(len(coins)):
        coin = coins[i]
        draw_circle(screen, PLAYER if i % 2 else AI, (coin.center.x, coin.center.y), radius, 2)

    for event in pygame.event.get():

        if event.type == pygame.QUIT:
            running = not running
            break

        if event.type == pygame.MOUSEBUTTONDOWN:
            position = event.pos

            if event.button == 1 and table.contains_point(position):
                coin = Circle(position, radius)
                coins.append(coin)

                if coin not in table or count_intersections(coins) != 0:
                    coins.pop()
                    continue

                # ai move
                coins.append(Circle(play_move(table, coins), radius))

        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                running = not running
                break

    pygame.display.update()
    clock.tick(60)

pygame.quit()

