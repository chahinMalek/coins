from math import sqrt, pi, cos, sin
from typing import Tuple, List

import numpy as np
from scipy.spatial import kdtree


class Circle(object):

    class Point(object):

        def __init__(self, x: int, y: int):
            self.x = x
            self.y = y

        def __iter__(self):
            return (self.__dict__[key] for key in sorted(self.__dict__))

        def __hash__(self):
            return (self.x, self.y).__hash__()

        def __lt__(self, other):
            return self.y < other.y or (self.y == other.y and self.x < other.x)

        def __gt__(self, other):
            return self.y > other.y or (self.y == other.y and self.x > other.x)

        def __eq__(self, other):
            return self.x == other.x and self.y == other.y

    def __init__(self, center: Tuple, radius: float):
        self.center = Circle.Point(*center)
        self.radius = radius

    def __rmul__(self, other):
        return Circle((self.center.x, self.center.y), other * self.radius)

    def __contains__(self, circle: 'Circle') -> bool:
        center = circle.center
        return sqrt((center.x-self.center.x) ** 2 + (center.y-self.center.y) ** 2) + circle.radius <= self.radius

    def __repr__(self):
        return '({}, {}), {}'.format(self.center.x, self.center.y, self.radius)

    def __str__(self):
        return self.__repr__()

    def contains_point(self, point: Tuple) -> bool:
        return Circle(point, 0) in self

    def intersects(self, other: 'Circle') -> bool:
        x1, y1 = self.center
        x2, y2 = other.center
        return sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2) <= self.radius + other.radius

    def area(self):
        return pi * self.radius * self.radius


def play_move(table: Circle, coins: List) -> Tuple[int, int]:

    if len(coins) == 0:
        return table.center.x, table.center.y

    coin_center = coins[-1].center
    center = table.center
    return coin_center.x + 2 * (center.x - coin_center.x), coin_center.y + 2 * (center.y - coin_center.y)


def has_moves_left(table: Circle, coins: List, radius: float) -> bool:

    points = np.array([(coin.center.x, coin.center.y) for coin in coins])
    tree = kdtree.KDTree(points)
    double_radius = 2 * radius

    for point in points:
        for angle in range(360):
            p = (point[0] + double_radius * cos(angle), point[1] + double_radius * sin(angle))

            if Circle(p, radius) in table and len(tree.query_ball_point(p, double_radius)) == 0:
                return True

    return False
