from typing import List

from bintrees import AVLTree as avl

from util import Circle


def count_intersections(circles: List[Circle]) -> int:

    start_points_map = {}
    end_points_map = {}

    for index, circle in enumerate(circles):
        start_points_map[(circle.center.x - circle.radius, circle.center.y)] = index
        end_points_map[(circle.center.x + circle.radius, circle.center.y)] = index

    start_points = sorted(start_points_map.keys(), reverse=True)
    end_points = sorted(end_points_map.keys(), reverse=True)

    tree = avl()
    intersection_set = set()

    while len(start_points) > 0 and len(end_points) > 0:

        if start_points[-1] <= end_points[-1]:
            circle = circles[start_points_map[start_points[-1]]]
            tree.insert(circle.center, circle)

            try:
                key = circle.center

                while True:
                    prev = tree.prev_item(key)[1]

                    if prev.intersects(circle):
                        intersection_set.add(prev.center)
                        intersection_set.add(circle.center)
                    else:
                        break

                    key = prev.center

            except KeyError:
                pass

            try:
                key = circle.center

                while True:
                    succ = tree.succ_item(key)[1]

                    if succ.intersects(circle):
                        intersection_set.add(succ.center)
                        intersection_set.add(circle.center)
                    else:
                        break

                    key = succ.center

            except KeyError:
                pass

            start_points.pop()

        else:
            tree.remove(circles[end_points_map[end_points[-1]]].center)
            end_points.pop()

    return len(intersection_set)
